<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Tugas PHP</title>
    </head>

    <body>

        <?php

        require("animal.php");
        require("Ape.php");
        require("Frog.php");

        echo "<h3>Latihan OOP PHP</h4>";
        echo "<h4>output akhir</h4>";

            //shaun
            $sheep = new animal("shaun");
            echo "Name : ". $sheep -> namaHewan . "<br>";
            echo "legs : ". $sheep -> legs . "<br>";
            echo "cold blooded : ". $sheep -> cold_blooded . "<br><br>";

            //Frog
            $kodok = new Frog("buduk");
            echo "Name : ". $kodok -> namaHewan . "<br>";
            echo "legs : ". $kodok -> legs . "<br>";
            echo "cold blooded : ". $kodok -> cold_blooded . "<br>";
            echo "Jump : ";
            $kodok -> jump(); 

            echo "<br><br>";

            //Ape
            $sungokong = new Ape("kera sakti");
            echo "Name : ". $sungokong -> namaHewan . "<br>";
            echo "legs : ". $sungokong -> legs . "<br>";
            echo "cold blooded : ". $sungokong -> cold_blooded . "<br>";
            echo "Yell : ";
            $sungokong -> yell() . "<br>";


        ?>
        
    </body>

</html>